= sendEmail

Para enviar un email utilizaremos el DSL `sendMail`

== Simple eMail

[source,groovy]
----
sendEmail {
    to 'me' //<1>
    from 'me'
    subject "Test"
    body """Hi
GroogleGmail service is sending
this email
    """
}
----
<1> Los campos `to` y `from` son obligatorios, el resto de campos son opcionales aunque es aconsejable
incluirlos en el envío del mail.

NOTE: En el caso de quere incluir más emails se escribirán separados por coma. Ejemplo:
'example1@gmail.com,example2@gmail.com,'

== eMail con formato

Por defecto el formato utilizado es texto plano (`text/plain`) pero
existe la posibilidad de indicar otros formatos al texto del correo, como puede ser formato `html`:

[source,groovy]
----
sendEmail {
    to 'me'
    from 'me'
    subject "Test"
    body """Hi <strong>GroogleGmail</strong> service is sending this email
    format "text/html" //<1>
    """
}
----
<1> indicamos el content-type que queremos aplicar

Este el email que recibiremos:

[quote, formato aplicado = text/html ]
Hi *GroogleGmail* service is sending this email



== Attachment

=== File

Podemos incluir un archivo adjunto en nuestro correo mediante `attachments` el cual nos permite
diferentes posibilidades

.Adjuntar un fichero
[source,java]
----
sendEmail {
    to 'me' //<1>
    from 'me'
    subject "Test"
    body """Hi
GroogleGmail service is sending
this email
    """
    attachments {
        file '/el/path/al/file.txt'
        file '/el/path/al/otro.txt' //<1>
        file '/el/path/al/otromas.txt'
    }
}
----
<1> Podemos adjuntar tantos `file` como queramos


== Directorio

Así mismo podemos adjuntar los ficheros de un directorio mediante `dir`

----
./testAttachment
    attachmentTxt.txt
    attachmentAsciidoc.adoc
----


[source,java]
----
sendEmail {
    to 'me'
    from 'me'
    subject "Test"
    body """Hi
GroogleGmail service is sending
this email
    """
    attachments{
        dir "./testAttachment"
    }
}
----

== Filtros

Si lo que queremos es enviar sólo ciertos ficheros de ese directorio podremos indicar un segundo argumento
a `dir` como expresión a evaluar

[source,java]
----
sendEmail {
    to 'me'
    from 'me'
    subject "Test"
    body """Hi
GroogleGmail service is sending
this email
    """
    attachments{
        dir "./testAttachment", ".*txt"
    }
}
----






