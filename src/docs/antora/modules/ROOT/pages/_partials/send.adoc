= sendEmail

Para enviar un email utilizaremos `sendMail`:

[#example,source,java]
----
sendEmail {
    to 'me' //<1>
    from 'me'
    subject "Test"
    body """Hi
GroogleGmail service is sending
this email
    """
}
----
<1> Los campos `to` y `from` son obligatorios, el resto de campos son opcionales aunque es aconsejable
incluirlos en el envío del mail.

NOTE: En el caso de quere incluir más emails se escribirán separados por coma. Ejemplo:
'example1@gmail.com,example2@gmail.com,'


== Attachment

Para incluir un archivo adjunto en nuestro correo deberemos utilizar el siguiente DSL


[#example,source,java]
----
sendEmail {
    to 'me' //<1>
    from 'me'
    subject "Test"
    body """Hi
GroogleGmail service is sending
this email
    """
    attachment attachmentFile
}
----

donde attachmentFile es un `java.io.File`

