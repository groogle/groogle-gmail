package com.puravida.groogle.test.gmail

import com.google.api.services.gmail.GmailScopes
import com.puravida.groogle.GmailService
import com.puravida.groogle.GmailServiceBuilder
import com.puravida.groogle.Groogle
import com.puravida.groogle.GroogleBuilder
import spock.lang.Specification
import spock.lang.Shared
import spock.lang.Unroll

import java.time.Instant
import java.time.temporal.ChronoUnit


class GroovyGmailBuilderSpec extends Specification {

    @Shared
    Groogle groogle

    static String DIR_ATTACHMENTS = './testAttachment'
    static String DIR_DOWNLOADS = './testDownload'

    @Shared
    String userMailTest = 'groovy.groogle@gmail.com'

    void setup(){
        //tag::register[]
        groogle = GroogleBuilder.build {
            withOAuthCredentials {
                applicationName 'test-gmail'
                withScopes GmailScopes.MAIL_GOOGLE_COM
                usingCredentials "/client_secret.json"
                storeCredentials true
            }
            register(GmailServiceBuilder.build(), GmailService)
        }
        //end::register[]
    }
    void cleanup(){
        new File(DIR_ATTACHMENTS).deleteDir()
        new File(DIR_DOWNLOADS).deleteDir()
    }
    def "service registered"(){
        given: "a gmail"

        GmailService service = groogle.service(GmailService)

        expect:
        service
    }

    def "send simple mail"(){
        given: "send mail"

        GmailService service = groogle.service(GmailService)

        service.sendEmail {
            to userMailTest
            from 'me'
            subject "Test"
            body """Hi
GroogleGmail service is sending 
this email directly dir a Spock test
            """
        }

        expect:
        service
    }
    def "send simple mail with attachment"(){
        given: "send mail"
        new File(DIR_ATTACHMENTS).mkdir()
        File attachmentFile = new File ("${DIR_ATTACHMENTS}/test.txt")
        attachmentFile << "send simple mail with attachments"
        GmailService service = groogle.service(GmailService)

        service.sendEmail {
            to userMailTest
            from 'me'
            subject "Test"
            body """Hi
<strong>GroogleGmail</strong> service is sending 
this email directly dir a Spock test
send simple mail with attachment
            """
            format "text/html"

            attachments {
                file attachmentFile
            }
        }

        expect:
        service

        and:
        attachmentFile.delete()
    }

    def "send simple mail with attachment folder"(){
        given: "a directory"
        new File(DIR_ATTACHMENTS).mkdir()

        and: "some files into it"

        File attachmentFile = new File ("${DIR_ATTACHMENTS}/test.txt")
        attachmentFile << "send simple mail with attachments"

        File antotherAttachmentFile = new File ("${DIR_ATTACHMENTS}/test2.txt")
        antotherAttachmentFile << "send simple mail with attachments"

        when: "send email"
        GmailService service = groogle.service(GmailService)

        service.sendEmail {
            to userMailTest
            from 'me'
            subject "Test"
            body """Hi
<strong>GroogleGmail</strong> service is sending 
this email directly dir a Spock test

send simple mail with attachment folder
            """
            format "text/html"
            attachments{
                dir DIR_ATTACHMENTS
            }

        }

        then:
        service
    }

    def "send simple mail with attachment folder with filter"(){
        given: "a directory"
        new File(DIR_ATTACHMENTS).mkdir()

        and: "some files into it"

        File attachmentFile = new File ("${DIR_ATTACHMENTS}/test.txt")
        attachmentFile << "send simple mail with attachments"

        File antotherAttachmentFile = new File ("${DIR_ATTACHMENTS}/test2.text")
        antotherAttachmentFile << "send simple mail with attachments"

        when: "send email"
        GmailService service = groogle.service(GmailService)

        service.sendEmail {
            to userMailTest
            from 'me'
            subject "Test"
            body """Hi
<strong>GroogleGmail</strong> service is sending 
this email directly dir a Spock test

send simple mail with attachment folder with filter

            """
            format "text/html"

            attachments{
                dir "./${DIR_ATTACHMENTS}", ".*txt"
            }
        }

        then:
        service
    }

    def "obtain messages with filter and read it"(){
        given: "a gmail service"

        GmailService service = groogle.service(GmailService)

        when: "find messages"
        //tag::filterFor[]
        int count = 0
        service.eachMessage(
                {
                    from 'me'
                    to   userMailTest
                    subject "Test"
                },
                {
                    println message.id
                    count++
                }
        )
        //end::filterFor[]

        then:
        count
    }

    def "obtain messages with filter and download it"(){
        given: "a gmail service"

        GmailService service = groogle.service(GmailService)

        when: "find messages"
        service.eachMessage(
                {
                    from 'me'
                    to   userMailTest
                    subject "Test"

                },
                {
                    downloadAttachments DIR_DOWNLOADS
                }
        )

        then:
        new File(DIR_DOWNLOADS).exists()

        and:
        new File(DIR_DOWNLOADS).listFiles()
    }

    def "obtain messages with filter by labelNames and download it"(){
        given: "a service"

        GmailService service = groogle.service(GmailService)

        when: "find messages"
        //tag::filterForLabel[]
        service.eachMessage(
                {
                    from 'me'
                    to   userMailTest
                    subject "Test"
                    labelNames "groogle, 101Script"
                },
                {
                    downloadAttachments DIR_DOWNLOADS
                }
        )
        //end::filterForLabel[]

        then:
        new File(DIR_DOWNLOADS).exists()

        and:
        new File(DIR_DOWNLOADS).listFiles()
    }


    def "obtain messages with filter extension"(){
        given: "a service"

        GmailService service = groogle.service(GmailService)

        when: "find messages"
        service.eachMessage(
                {
                    from 'me'
                    to   userMailTest
                    subject "Test"
                },
                {
                    downloadAttachmentsIfExtension DIR_DOWNLOADS, ".txt,.excel"
                }
        )

        then:
        new File(DIR_DOWNLOADS).exists()

        and:
        new File(DIR_DOWNLOADS).listFiles()
    }


    def "obtain messages with filter after date and before date"(){
        given: "a service"
        GmailService service = groogle.service(GmailService)

        and: "some dates"
        Date dayAfter = Instant.now().minus(100L, ChronoUnit.DAYS).toDate()
        Date datBefore = Instant.now().plus(1L, ChronoUnit.DAYS).toDate()

        when: "find messages"
        //tag::filterForDate[]
        service.eachMessage(
                {
                    from 'me'
                    after dayAfter
                    before datBefore

                },
                {
                    println message.id
                }
        )
        //tag::filterForDate[]

        then:
        service
    }

    @Unroll
    def "obtain messages with filter unRead #unReadValue"(){
        given: "find mail"

        GmailService service = groogle.service(GmailService)
//tag::filterForUnread[]
        service.eachMessage(
                {
                    from fromValue
                    unRead unReadValue

                },{

                }
        )
//end::filterForUnread[]
        expect:
        service

        where:
        unReadValue | fromValue
        true        | 'me'
        false       | userMailTest
    }


    def "obtain messages with filter inChat"(){
        given: "a service"
        GmailService service = groogle.service(GmailService)
        Date dayAfter = Instant.now().minus(1L, ChronoUnit.DAYS).toDate()
        when: "find messages"
        //tag::filterForInChat[]
        service.eachMessage(
                {
                    from 'me'
                    inChat true
                    after dayAfter
                },
                {
                    println message.id
                }
        )
        //end::filterForInChat[]

        then:
        service
    }

    @Unroll
    def "obtain messages with filter larguer"(){
        given: "a service"
        GmailService service = groogle.service(GmailService)
        Date dayAfter = Instant.now().minus(1L, ChronoUnit.DAYS).toDate()
        when: "find messages"

        //tag::filterForInSize[]
        service.eachMessage(
                {
                    from 'me'
                    larger filterLarger
                    after dayAfter
                },
                {
                    println message.id
                }
        )
        //end::filterForInSize[]

        then:
        service
        where:
        filterLarger << ['50M', '50', '50K']
    }
    @Unroll
    def "obtain messages with filter smaller"(){
        given: "a service"
        GmailService service = groogle.service(GmailService)
        Date dayAfter = Instant.now().minus(1L, ChronoUnit.DAYS).toDate()

        when: "find messages"
        //tag::filterForInSize[]
        service.eachMessage(
                {
                    from 'me'
                    smaller filterSmaller
                    after dayAfter
                },
                {
                    println message.id
                }
        )
        //end::filterForInSize[]

        then:
        service
        where:
        filterSmaller << ['50M', '50', '50K']
    }

    def "obtain messages with filter and delete it"(){
        given: "find mail"
        Date dayAfter = Instant.now().minus(1L, ChronoUnit.DAYS).toDate()
        println "dayAfter ${dayAfter}"
        GmailService service = groogle.service(GmailService)
//tag::filterForDelete[]
        service.eachMessage(
                {
                    from 'me'
                    subject "Test"
                    hasAttachments true
                    after dayAfter
                },{
                    delete()
                }
        )
//end::filterForDelete[]
        expect:
        service
    }
    def "obtain messages with filter hasAttachments"(){
        given: "find mail"
        Date dayAfter = Instant.now().minus(1L, ChronoUnit.DAYS).toDate()
        println "dayAfter ${dayAfter}"
        GmailService service = groogle.service(GmailService)
//tag::filterForHasAttachments []
        service.eachMessage(
                {
                    from 'me'
                    subject "Test"
                    hasAttachments true
                    after dayAfter

                },{
                    println message.id
                }
        )
//end::filterForHasAttachments []
        expect:
        service
    }


    def "obtain the body message"(){
        given: "find mail"
        Date dayAfter = Instant.now().minus(1L, ChronoUnit.DAYS).toDate()
        println "dayAfter ${dayAfter}"
        GmailService service = groogle.service(GmailService)
//tag::showBody[]
        service.eachMessage(
                {
                    from 'me'
                    subject "Test"
                    hasAttachments false
                    after dayAfter

                },{
                    println body
                }
        )
//end::showBody[]
        expect:
        service
    }

}
